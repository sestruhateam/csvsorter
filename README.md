## Task

* [Text task](https://docs.google.com/document/d/1fOCWwDi43Oe3fjXVG29dUqaZEFepSJS8dfMojH1hr0o/edit)

## Used

Project used [Apache Maven framework](https://ru.wikipedia.org/wiki/Apache_Maven), java 1.8.0_181

### Install ubuntu

``` bash
sudo apt update && sudo apt -y install default-jdk maven 
```

### Start

Build:

``` bash
git clone https://bitbucket.org/sestruhateam/csvsorter && cd ~/csvsorter && mvn package && cd target
```

And run:

``` bash
java -jar CSVSorter-jar-with-dependencies.jar ~/PATH_TO_CSV_SET_DIR
```