package ru.CSVSorter;

import ru.CSVSorter.services.ProductReader;
import ru.CSVSorter.utils.CSVUtils;

import java.io.FileNotFoundException;

public class Main {
    private static final String HELP = "use : java -jar NAME.jar /home/user/DIR_WITH_CSV";
    public static final String DEFAULT_TEMPLATE_PATH = "set";
    public static final String DEFAULT_RESULT_PATH = "result.csv";
    public static final boolean DEBUG = false;

    public Main(String args[]) {
        if (!DEBUG) {
            if (args.length <= 0) {
                System.out.println(HELP);
                System.exit(0);
            }
            System.out.println("path:" + args[0]);
        }

        long start = System.nanoTime();

        //CSVUtils.generateRandomFiles(500);

        try {
            new ProductReader((DEBUG ? DEFAULT_TEMPLATE_PATH : args[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Time of exec:" + (System.nanoTime() - start) / (1000_000_000) + "s");
    }

    public static void main(String args[]) {
        new Main(args);
    }
}
