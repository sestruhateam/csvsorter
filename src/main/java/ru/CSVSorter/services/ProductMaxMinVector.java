package ru.CSVSorter.services;

import ru.CSVSorter.models.Product;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class ProductMaxMinVector extends Vector<Product> {
    /**
     * Comparator product by price
     */
    private final Comparator<Product> comparableProductByPrice = (o1, o2) -> Float.compare(o1.getPrice(), o2.getPrice());

    /**
     * Return Comparator product by price
     *
     * @return Comparator<Product> - Comparator product by price
     */
    public Comparator<Product> getComparableProductByPrice() {
        return comparableProductByPrice;
    }

    /**
     * Add product to collection with binary search suitable position
     *
     * @param product - element to be included in the collection
     */
    public synchronized void addWithSort(Product product) {
        int insertedIndex = Collections.binarySearch(this, product, comparableProductByPrice);
        if (insertedIndex < 0) {
            super.add((-(insertedIndex) - 1), product);
        } else {
            super.add(insertedIndex, product);
        }
    }

    /**
     * Return maximum price value from collection
     *
     * @return float - price value last element from collection
     */
    public float getMax() {
        if (size() <= 0) {
            return Float.MIN_VALUE;
        }
        return getLast().getPrice();
    }

    /**
     * Return minimal price value from collection
     *
     * @return float - price value first element from collection
     */
    public float getMin() {
        if (size() <= 0) {
            return Float.MAX_VALUE;
        }

        return get(0).getPrice();
    }

    /**
     * Remove last element from collection
     *
     * @return true if elem has been removed
     */
    public synchronized boolean removeLast() {
        if (size() <= 0) {
            return false;
        }

        return remove(size() - 1) != null;
    }

    /**
     * Return last element from collection
     *
     * @return Product
     */
    public Product getLast() {
        if (size() <= 0) {
            return null;
        }

        return get(size() - 1);
    }
}
