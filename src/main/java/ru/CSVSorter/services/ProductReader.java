package ru.CSVSorter.services;

import com.opencsv.bean.CsvToBeanBuilder;
import ru.CSVSorter.models.Product;
import ru.CSVSorter.utils.CSVUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static ru.CSVSorter.Main.DEFAULT_RESULT_PATH;

/**
 * Class allowing parse .csv files by {@link ru.CSVSorter.models.Product} model
 */
public class ProductReader {
    /**
     * Main filtered list products
     */
    private final List products = new FilteredVector();

    /**
     * Thread pool, the count of thread is determined by the processor cores.
     */
    private final ExecutorService threadPool = Executors.newFixedThreadPool(2 * Runtime.getRuntime().availableProcessors() + 1);

    /**
     * An additional map for each of the {@link ru.CSVSorter.models.Product}  IDs.
     * Defining ID and list of {@link ru.CSVSorter.models.Product}  entities.
     */
    private final Map productsIds = Collections.synchronizedMap(new HashMap<Integer, ProductMaxMinVector>());

    /**
     * Id`s counter products
     */
    private volatile long globalIds = 0;

    /**
     * Auxiliary vector with filtered logic.
     */
    class FilteredVector extends ProductMaxMinVector {
        /**
         * Max size {@link #products} list
         */
        private final static int MAX_SIZE = 1000;

        /**
         * Max size {@link #productsIds} map
         */
        private final static int MAX_EQUALS_ELEMS_SIZE = 20;

        @Override
        public synchronized boolean add(Product product) {
            Object value = productsIds.get(product.getId());

            if (value == null) {
                ProductMaxMinVector ids = new ProductMaxMinVector();

                ids.add(product);
                productsIds.put(product.getId(), ids);

            } else {
                ProductMaxMinVector ids = (ProductMaxMinVector) value;

                if (ids.size() >= MAX_EQUALS_ELEMS_SIZE) {
                    if (product.getPrice() < ids.getMax()) {
                        super.remove(ids.getLast());
                        ids.removeLast();
                        ids.addWithSort(product);
                    } else {
                        return false;
                    }
                } else {
                    ids.addWithSort(product);
                }
            }

            if (this.size() + 1 >= MAX_SIZE) {
                if (product.getPrice() < this.getMax()) {
                    this.removeLast();
                } else {
                    return false;
                }
            }

            this.addWithSort(product);

            return true;
        }
    }

    /**
     * Initialize class and start parsing. In the end - creates .csv result file.
     *
     * @param filePath - root directory included .csv files
     * @throws FileNotFoundException
     */
    public ProductReader(String filePath) throws FileNotFoundException {

        File root = new File(filePath);
        if (root.isDirectory() & root.exists()) {
            System.out.println("Start parse catalog..");
            recursiveSearch(root);
        } else {
            throw new FileNotFoundException();
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Generate CSV file(" + DEFAULT_RESULT_PATH + ") to current directory ...");
        CSVUtils.createCSVFileByProducts(products, DEFAULT_RESULT_PATH);
    }

    /**
     * Recursive search .csv files, with parallel parsing
     *
     * @param root - start directory included .csv files
     */
    private void recursiveSearch(File root) {

        if (root.isDirectory()) {
            File files[] = root.listFiles();
            if (files != null) {
                for (File currentFile : files) {
                    recursiveSearch(currentFile);
                }
            }
        } else if (root.isFile()) {
            threadPool.execute(() -> {
                try (Reader fileReader = new FileReader(root)) {

                    Iterator<Product> productIterator = new CsvToBeanBuilder(fileReader).withType(Product.class).build().iterator();

                    Product currentElem;
                    while (productIterator.hasNext()) {
                        currentElem = productIterator.next();
                        currentElem.setUniqueId(globalIds++);
                        products.add(currentElem);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}