package ru.CSVSorter.utils;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import ru.CSVSorter.models.Product;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static ru.CSVSorter.Main.DEFAULT_TEMPLATE_PATH;

public class CSVUtils {

    /**
     * Generate random file to {@link ru.CSVSorter.Main#DEFAULT_TEMPLATE_PATH} path.
     * Any file include between 100 - 1000 rows.
     *
     * @param fileCounts - number of files generated
     */
    public static void generateRandomFiles(int fileCounts) {

        final ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        if (!Files.exists(Paths.get(DEFAULT_TEMPLATE_PATH))) {
            try {
                Files.createDirectories(Paths.get(DEFAULT_TEMPLATE_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i1 = 0; i1 < fileCounts; i1++) {

            final String fileName = DEFAULT_TEMPLATE_PATH + File.separator + "file" + i1 + ".csv";

            threadPool.execute(() -> {
                final int rowCounts = ThreadLocalRandom.current().nextInt(100, 1000 + 1);

                try (Writer writer = new FileWriter(fileName)) {

                    StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer).withSeparator(CSVWriter.DEFAULT_SEPARATOR).build();

                    for (int i2 = 0; i2 < rowCounts; i2++) {
                        int id = ThreadLocalRandom.current().nextInt(1, 1001);
                        String name = "productName" + id;
                        String condition = "condition" + id;
                        String state = "state" + id;

                        float price = 1.0f + ThreadLocalRandom.current().nextFloat() * (30000.0f - 1.0f);

                        sbc.write(new Product(id, name, condition, state, price));
                    }

                } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
                    e.printStackTrace();
                }
            });
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert list products to .csv file.
     *
     * @param products - list entity {@link ru.CSVSorter.models.Product}
     * @param fileName - path to result file
     */
    public static void createCSVFileByProducts(List<Product> products, String fileName) {
        try (Writer writer = new FileWriter(fileName)) {

            StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer).withSeparator(CSVWriter.DEFAULT_SEPARATOR).build();
            sbc.write(products);

        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        }
    }
}
