package ru.CSVSorter.models;

import com.opencsv.bean.CsvBindByName;

/**
 * Product display entity
 * <p>
 * > Each file contains 5 columns:
 * > product ID (integer),
 * > Name (string),
 * > Condition (string),
 * > State (string),
 * > Price (float).
 */
public class Product {
    @CsvBindByName(column = "id", required = true)
    private int id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "condition", required = true)
    private String condition;

    @CsvBindByName(column = "state", required = true)
    private String state;

    @CsvBindByName(column = "price", required = true)
    private float price;

    private long uniqueId;

    public Product(int id, String name, String condition, String state, float price) {
        this.id = id;
        this.name = name;
        this.condition = condition;
        this.state = state;
        this.price = price;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.condition = "";
        this.state = "";
        this.price = -1.0f;
    }

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", condition='" + condition + '\'' +
                ", state='" + state + '\'' +
                ", price=" + price +
                ", uniqueId=" + uniqueId +
                '}';
    }
}
